# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# ab <g1@borowicz.info>, 2015
# Andrew McMillan <andrew@mcmillan.net.nz>, 2011
# Cyril Giraud <cgiraud@free.fr>, 2017
# dziobak <dziobak84@interia.pl>, 2011
# silk <silk@boktor.net>, 2011
# spasstl <marty@xemail.de>, 2011
msgid ""
msgstr ""
"Project-Id-Version: DAViCal\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-01-14 23:06+0100\n"
"PO-Revision-Date: 2017-01-15 20:36+0000\n"
"Last-Translator: Cyril Giraud <cgiraud@free.fr>\n"
"Language-Team: Polish (http://www.transifex.com/lkraav/davical/language/pl/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: pl\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. Translators: this is the formatting of a date with time. See
#. http://php.net/manual/en/function.strftime.php
msgid "%F %T"
msgstr "%F %T"

#. Translators: his is the formatting of just the time. See
#. http://php.net/manual/en/function.strftime.php
msgid "%T"
msgstr "%T"

msgid "*** Default Locale ***"
msgstr "*** Domyślne ustawienia lokalne ***"

msgid "*** Unknown ***"
msgstr "*** Nieznane ***"

#, php-format
msgid "- adding %s to group : %s"
msgstr "Dodaj %s do grupy: %s"

#, php-format
msgid "- adding users %s to group : %s"
msgstr "Dodaj Użytkownika %s do grupy: %s"

#, php-format
msgid "- creating groups : %s"
msgstr "- tworzenie grup: %s"

#, php-format
msgid "- creating record for users :  %s"
msgstr "- tworzenie rekordów użytkowników: %s"

#, php-format
msgid "- deactivate groups : %s"
msgstr "- wyłącz grupy: %s"

#, php-format
msgid "- deactivating users : %s"
msgstr "- wyłączenie użytkowników: %s"

#, php-format
msgid "- nothing done on : %s"
msgstr "nic nie wykonano na: %s"

#, php-format
msgid "- removing %s from group : %s"
msgstr "- usuwanie %s z grupy: %s"

#, php-format
msgid "- updating groups : %s"
msgstr "- aktualizacja grup: %s"

#, php-format
msgid "- updating user records : %s"
msgstr "- aktualizacja rekordów użytkownika: %s"

#, php-format
msgid ""
"<a href=\"https://wiki.davical.org/w/Setup_Failure_Codes/%s\">Explanation on"
" DAViCal Wiki</a>"
msgstr ""

msgid ""
"<b>WARNING: all events in this path will be deleted before inserting allof "
"the ics file</b>"
msgstr "<b>OSTRZEŻENIE: wszystkie zdarzenia z tej ścieżki zostaną usunięte przed wstawieniem plików ICS</b>"

#, php-format
msgid ""
"<h1>Help</h1>\n"
"<p>For initial help you should visit the <a href=\"https://www.davical.org/\" target=\"_blank\">DAViCal Home Page</a> or take\n"
"a look at the <a href=\"https://wiki.davical.org/%s\" target=\"_blank\">DAViCal Wiki</a>.</p>\n"
"<p>If you can't find the answers there, visit us on <a href=\"https://wikipedia.org/wiki/Internet_Relay_Chat\" target=\"_blank\">IRC</a> in\n"
"the <b>#davical</b> channel on <a href=\"https://www.oftc.net/\" target=\"_blank\">irc.oftc.net</a>,\n"
"or send a question to the <a href=\"https://lists.sourceforge.net/mailman/listinfo/davical-general\" target=\"_blank\">DAViCal Users mailing list</a>.</p>\n"
"<p>The <a href=\"https://sourceforge.net/p/davical/mailman/davical-general/\" title=\"DAViCal Users Mailing List\" target=\"_blank\">mailing list\n"
"archives can be helpful too.</p>"
msgstr ""

#, php-format
msgid ""
"<h1>Log On Please</h1><p>For access to the %s you should log on withthe "
"username and password that have been issued to you.</p><p>If you would like "
"to request access, please e-mail %s.</p>"
msgstr "<h1>Zaloguj się</h1><p>Aby uzyskać dostęp do %s powinieneś zalogować się swoją nazwą użytkownika i hasłem.</p><p>Jeśli chcesz uzyskać dostęp, wyślij e-mail do %s.</p>"

msgid "A DAViCal principal collection may only contain collections"
msgstr ""

msgid "A collection already exists at that location."
msgstr "Zbiór już istnieje w tej lokalizacji."

msgid "A collection may not be both a calendar and an addressbook."
msgstr ""

msgid "A resource already exists at the destination."
msgstr "Zasób już istnieje w lokalizacji docelowej."

msgid "AWL Library version "
msgstr "Wersja biblioteki AWL "

msgid "Access Tickets"
msgstr "Możliwość dostępu"

msgid "Access ticket deleted."
msgstr "Możliwość dostępu została usunięta."

msgid "Action"
msgstr "Akcja"

msgid "Active"
msgstr "Aktywny"

msgid "Adding new member to this Group Principal"
msgstr ""

#. Translators: in the sense of 'systems admin'
msgid "Admin"
msgstr "Administrator"

msgid "Administration"
msgstr "Administracja"

msgid "Administration Functions"
msgstr ""

msgid "Administrator"
msgstr "Administrator"

msgid "All"
msgstr "Wszystko"

msgid "All collection data will be unrecoverably deleted."
msgstr "Wszystkie dane kolekcji zostaną nieodwracalnie usunięte."

#, php-format
msgid "All events of user \"%s\" were deleted and replaced by those from file %s"
msgstr ""

msgid ""
"All of the principal's calendars and events will be unrecoverably deleted."
msgstr ""

msgid "All privileges"
msgstr "Wszystkie uprawnienia"

msgid "All requested changes were made."
msgstr "Wszystkie zażądane zmiany zostały wprowadzone"

msgid ""
"Allow free/busy enquiries targeted at the owner of this scheduling inbox"
msgstr "Zezwól na pytania free/busy skierowane do właściciela tej skrzynki harmonogramu"

msgid "An \"Administrator\" user has full rights to the whole DAViCal System"
msgstr "Użytkownik \"Administrator\" ma pełne prawa do całego systemu DAViCal"

msgid "Anonymous users are not allowed to modify calendars"
msgstr "Użytkownicy anonimowi nie mają pozwolenia na modyfikację kalendarzy"

msgid "Anonymous users may only access public calendars"
msgstr "Użytkownicy anonimowi mogą uzyskać dostęp tylko do publicznych kalendarzy"

msgid "Append"
msgstr "Dodaj"

msgid "Application DB User"
msgstr "Użytkownik DB aplikacji"

msgid "Apply Changes"
msgstr "Zatwierdź zmiany"

msgid "Apply DB Patches"
msgstr "Zastosuj uaktoalnienia DB"

msgid "Attachment"
msgstr "Załącznik"

msgid "Attention: email address not unique, scheduling may not work!"
msgstr ""

msgid "Authentication server unavailable."
msgstr ""

msgid "Binding deleted."
msgstr ""

msgid "Bindings to other collections"
msgstr ""

msgid "Bindings to this Collection"
msgstr ""

msgid "Bindings to this Principal's Collections"
msgstr ""

msgid "Body contains no XML data!"
msgstr ""

msgid "Bound As"
msgstr ""

msgid "Bound As is invalid"
msgstr ""

msgid "Browse all users"
msgstr "Przeglądaj wszystkich użytkowników"

msgid "Busy"
msgstr "Zajęty"

#, php-format
msgid "Calendar \"%s\" was loaded from file."
msgstr "Kalendarz @%s@ władowny z pliku"

msgid "Calendar Principals"
msgstr ""

msgid "Calendar Timezone"
msgstr "Strefa czasowa kalendarza"

msgid "Can only add tickets for existing collection paths which you own"
msgstr ""

msgid "Can only bind collections into the current principal's namespace"
msgstr ""

msgid ""
"Cannot determine upstream version, because PHP has set “<a "
"href=\"https://secure.php.net/manual/en/filesystem.configuration.php#ini"
".allow-url-fopen\"><code>allow_url_fopen</code></a>” to "
"“<code>FALSE</code>”."
msgstr ""

msgid "Categories"
msgstr "Kategorie"

msgid "Change Password"
msgstr "Zmień hasło"

msgid "Click to display user details"
msgstr "Kliknij aby wyświetlić dane użytkownika"

msgid "Click to edit collection details"
msgstr ""

msgid "Click to edit principal details"
msgstr ""

msgid "Collection"
msgstr "Kolekcja"

msgid "Collection Grants"
msgstr "Prawa kolekcji"

msgid "Collection ID"
msgstr "ID kolekcji"

msgid "Collection deleted."
msgstr "Kolekcja została usunięta."

msgid ""
"Collections may not be both CalDAV calendars and CardDAV addressbooks at the"
" same time"
msgstr ""

msgid "Configuring Calendar Clients for DAViCal"
msgstr "Konfiguracja klientów kalendarza dla DAViCal"

msgid "Configuring DAViCal"
msgstr "Konfiguracja DAViCal"

msgid "Confirm"
msgstr "Potwierdź"

msgid "Confirm Deletion of the Binding"
msgstr ""

msgid "Confirm Deletion of the Collection"
msgstr "Potwierdź usunięcie kolekcji"

msgid "Confirm Deletion of the Principal"
msgstr ""

msgid "Confirm Deletion of the Ticket"
msgstr ""

msgid "Confirm Password"
msgstr "Potwierdź hasło"

msgid "Confirm the new password."
msgstr "Potwierdź nowe hasło"

msgid "Connection Parameters"
msgstr ""

msgid "Could not retrieve"
msgstr ""

msgid "Create"
msgstr "Utwórz"

msgid "Create Collection"
msgstr "Utwórz kolekcję"

msgid "Create Events/Collections"
msgstr "Utwórz zdarzenia/kolekcje"

msgid "Create New Collection"
msgstr "Utwórz nową kolekcję"

msgid "Create New Principal"
msgstr ""

msgid "Create Principal"
msgstr ""

msgid "Create a new principal (i.e. a new user, resource or group)"
msgstr ""

msgid "Create a resource or collection"
msgstr ""

msgid "Creating new Collection."
msgstr "Tworzenie nowej kolejcji"

msgid "Creating new Principal record."
msgstr ""

msgid "Creating new ticket granting privileges to this Principal"
msgstr ""

msgid "Current DAViCal version "
msgstr "Atkualna wersja DAViCal"

msgid ""
"Currently this page does nothing. Suggestions or patches to make it do "
"something useful will be gratefully received."
msgstr ""

msgid "DAV Path"
msgstr "Ścieżka DAV"

msgid ""
"DAV::resourcetype may only be set to a new value, it may not be removed."
msgstr ""

msgid "DAViCal CalDAV Server"
msgstr "DAViCal CalDAV Server"

msgid "DAViCal CalDAV Server - Configuration Help"
msgstr ""

msgid "DAViCal DB Schema version "
msgstr "Wersja schematu DB DAViCal"

msgid "DAViCal Fatal Error"
msgstr ""

msgid "DAViCal Homepage"
msgstr "Strona główna DAViCal"

msgid "DAViCal Wiki"
msgstr "DAViCal Wiki"

msgid "DAViCal only allows BIND requests for collections at present."
msgstr ""

msgid "DKIM signature missing"
msgstr ""

msgid "DKIM signature validation failed(DNS ERROR)"
msgstr ""

msgid "DKIM signature validation failed(KEY Parse ERROR)"
msgstr ""

msgid "DKIM signature validation failed(KEY Validation ERROR)"
msgstr ""

msgid "DKIM signature validation failed(Signature verification ERROR)"
msgstr ""

msgid "Database Error"
msgstr "Błąd bazy danych"

msgid "Database Host"
msgstr "Host bazy danych"

msgid "Database Name"
msgstr "Nazwa bazy danych"

msgid "Database Owner"
msgstr "Właściciel bazy danych"

msgid "Database Password"
msgstr "Hasło bazy danych"

msgid "Database Port"
msgstr "Port bazy danych"

msgid "Database Username"
msgstr "Nazwa użytkownika bazy danych"

msgid "Database error"
msgstr "Błąd bazy danych"

msgid "Database is Connected"
msgstr ""

msgid "Date Format Style"
msgstr "Styl formatowania daty"

msgid "Date Style"
msgstr "Format daty"

msgid "Default Privileges"
msgstr "Domyślne uprawnienia"

msgid "Default relationships added."
msgstr ""

msgid "Delete"
msgstr "Usuń"

msgid "Delete Events/Collections"
msgstr ""

msgid "Delete Principal"
msgstr ""

msgid "Delete a resource or collection"
msgstr ""

msgid "Deleted a grant from this Principal"
msgstr ""

msgid "Deleting Binding:"
msgstr ""

msgid "Deleting Collection:"
msgstr ""

msgid "Deleting Principal:"
msgstr ""

msgid "Deleting Ticket:"
msgstr ""

msgid ""
"Deliver scheduling invitations from an organiser to this scheduling inbox"
msgstr ""

msgid "Deliver scheduling replies from an attendee to this scheduling inbox"
msgstr ""

msgid "Dependencies"
msgstr ""

msgid "Dependency"
msgstr ""

msgid "Description"
msgstr ""

msgid "Destination collection does not exist"
msgstr ""

msgid "Directory on the server"
msgstr ""

msgid "Display Name"
msgstr ""

msgid "Displayname"
msgstr ""

msgid "Does the user have the right to perform this role?"
msgstr "Czy użytkownik ma prawo wykonywać tę rolę?"

msgid "Domain"
msgstr ""

msgid "Download entire collection as .ics file"
msgstr ""

msgid "EMail"
msgstr "E-mail"

msgid "EMail OK"
msgstr "E-mail zweryfikowano"

msgid "ERROR"
msgstr ""

msgid "ERROR: The full name may not be blank."
msgstr ""

msgid "ERROR: The new password must match the confirmed password."
msgstr "ERROR: Podane hasła nie pasują do siebie"

msgid "ERROR: There was a database error writing the roles information!"
msgstr "Błąd: Wystąpił błąd bazy danych podczas zapisywania informacji o roli."

msgid "Edit"
msgstr "Edytuj"

msgid "Edit this user record"
msgstr "Zmień dane tego użytkownika "

msgid "Email Address"
msgstr "Adres Email"

msgid ""
"Enter a username, if you know it, and click here, to be e-mailed a temporary"
" password."
msgstr "Wpisz nazwę użytkownika (jeśli ją znasz) i naciśnij tutaj aby otrzymać emailem tymczasowe hasło."

msgid "Enter your username and password then click here to log in."
msgstr "Wpisz nazwę użytkownika oraz hasło i naciśnij tutaj aby się zalogować."

#, php-format
msgid "Error NoGroupFound with filter >%s<, attributes >%s< , dn >%s<"
msgstr ""

#, php-format
msgid "Error NoUserFound with filter >%s<, attributes >%s< , dn >%s<"
msgstr ""

msgid "Error querying database."
msgstr "Błąd odpytywania bazy danych."

msgid "Error writing calendar details to database."
msgstr "Błąd zapisu kalendarza do bazy."

msgid "Error writing calendar properties to database."
msgstr ""

msgid "European"
msgstr "Europejski"

msgid "European (d/m/y)"
msgstr "Europejski (d/m/r)"

msgid "Existing resource does not match \"If-Match\" header - not accepted."
msgstr ""

msgid "Existing resource matches \"If-None-Match\" header - not accepted."
msgstr ""

msgid "Expires"
msgstr "Wygasa"

msgid "External Calendars"
msgstr ""

msgid "External Url"
msgstr ""

msgid "Fail"
msgstr ""

msgid "Failed to write collection."
msgstr ""

msgid "Feeds are only supported for calendars at present."
msgstr ""

msgid "For access to the"
msgstr "Aby uzyskać dostęp do"

msgid "Forbidden"
msgstr "Zabronione"

msgid "Free/Busy"
msgstr "Free/Busy"

msgid "Full Name"
msgstr "Imię i nazwisko"

msgid "Fullname"
msgstr "Pełna nazwa"

msgid "GET requests on collections are only supported for calendars."
msgstr ""

msgid "GNU gettext support"
msgstr ""

msgid "GO!"
msgstr "Uruchom!"

msgid "Go to the DAViCal Feature Requests"
msgstr ""

msgid "Grant"
msgstr ""

msgid "Granting new privileges from this Principal"
msgstr ""

msgid "Grants specify the access rights to a collection or a principal"
msgstr ""

#. Translators: in the sense of a group of people
msgid "Group"
msgstr "Grupa"

msgid "Group Members"
msgstr "Członkowie grupy"

msgid "Group Memberships"
msgstr "Członkostwo grupy"

msgid "Group Principals"
msgstr ""

msgid "Groups &amp; Grants"
msgstr ""

msgid ""
"Groups allow those granted rights to be assigned to a set of many principals"
" in one action"
msgstr ""

msgid ""
"Groups may be members of other groups, but complex nesting will hurt system "
"performance"
msgstr ""

msgid ""
"Groups provide an intermediate linking to minimise administration overhead."
"  They might not have calendars, and they will not usually log on."
msgstr ""

msgid "Has Members"
msgstr "Ma członków"

msgid "Help"
msgstr "Pomoc"

msgid "Help on the current screen"
msgstr ""

msgid "Help! I've forgotten my password!"
msgstr "Pomocy! Zapomniałem hasła!"

msgid "Here is a list of users (maybe :-)"
msgstr ""

msgid "Home"
msgstr "Początek"

msgid "Home "
msgstr ""

msgid "Home Page"
msgstr "Strona główna"

msgid "ID"
msgstr "ID"

msgid "ISO Format"
msgstr "Format ICS"

msgid "ISO Format (YYYY-MM-DD)"
msgstr "ISO (RRRR-MM-DD)"

#. Translators: short for 'Identifier'
msgid "Id"
msgstr "Id"

msgid "If you can read this then things must be mostly working already."
msgstr ""

msgid "If you have forgotten your password then"
msgstr "Jeśli zapomniałeś hasła, "

msgid "If you would like to request access, please e-mail"
msgstr "Jeśli chcesz poprosić o dostęp, wyślij e-mail do"

msgid "Import all .ics files of a directory"
msgstr "Importuj wszystkie pliki ICS z katalogu"

msgid "Import calendars and Synchronise LDAP."
msgstr ""

msgid ""
"In due course this program will implement the functionality which is "
"currently contained in that script, but until then I'm afraid you do need to"
" run it."
msgstr ""

msgid "Inactive Principals"
msgstr ""

msgid "Incorrect content type for addressbook: "
msgstr ""

msgid "Incorrect content type for calendar: "
msgstr ""

msgid "Invalid user name or password."
msgstr "Błędna nazwa użytkownika lub hasło."

msgid "Invalid username or password."
msgstr "Błędna nazwa użytkownika lub hasło"

msgid "Is Member of"
msgstr "Jest członkiem"

msgid "Is a Calendar"
msgstr "Jest kalendarzem"

msgid "Is an Addressbook"
msgstr "Jest książką adresową"

msgid "Is this user active?"
msgstr "Aktywny użytkownik?"

msgid "Items in Collection"
msgstr ""

msgid "Joined"
msgstr "Dodano"

msgid "Language"
msgstr "Język"

msgid "Last used"
msgstr "Ostatnio używany"

msgid "List External Calendars"
msgstr ""

msgid "List Groups"
msgstr ""

msgid "List Resources"
msgstr ""

msgid "List Users"
msgstr ""

msgid "Load From File"
msgstr ""

msgid "Locale"
msgstr ""

msgid "Location"
msgstr "Lokalizacja"

msgid "Log On Please"
msgstr "Proszę się zalogować"

msgid "Log out of DAViCal"
msgstr "Wyloguj z DAViCal"

msgid "Logout"
msgstr "Wyloguj się"

msgid "Member deleted from this Group Principal"
msgstr ""

msgid ""
"Most of DAViCal will work but upgrading to PHP 5.2 or later is strongly "
"recommended."
msgstr ""

msgid "Name"
msgstr "Nazwa"

msgid "New Collection"
msgstr "Nowa kolekcja"

msgid "New Password"
msgstr "Nowe hasło"

msgid "New Principal"
msgstr ""

#. Translators: not 'Yes'
msgid "No"
msgstr "Nie"

msgid "No calendar content"
msgstr ""

msgid "No collection found at that location."
msgstr "Nie znaleziono zbioru w lokalizacji"

msgid "No resource exists at the destination."
msgstr ""

msgid "No summary"
msgstr ""

#. Translators: short for 'Number'
msgid "No."
msgstr "Nr"

msgid "No. of Collections"
msgstr "Nr. kolekcji"

msgid "No. of Principals"
msgstr ""

msgid "No. of Resources"
msgstr ""

msgid "Not overwriting existing destination resource"
msgstr ""

msgid "Opaque"
msgstr ""

msgid "Operation Parameters"
msgstr ""

msgid "Organizer Missing"
msgstr ""

msgid "Override a Lock"
msgstr ""

msgid "PDO PostgreSQL drivers"
msgstr ""

msgid "PHP DateTime class"
msgstr ""

msgid "PHP Information"
msgstr ""

msgid "PHP LDAP module available"
msgstr ""

msgid "PHP Magic Quotes GPC off"
msgstr ""

msgid "PHP Magic Quotes runtime off"
msgstr ""

msgid "PHP PDO module available"
msgstr ""

msgid "PHP XML support"
msgstr ""

msgid "PHP calendar extension available"
msgstr ""

msgid "PHP curl support"
msgstr ""

msgid "PHP iconv support"
msgstr ""

msgid "PHP not using Apache Filter mode"
msgstr ""

msgid "PHP curl support is required for external binds"
msgstr ""

msgid ""
"PUT on a collection is only allowed for text/calendar content against a "
"calendar collection"
msgstr ""

msgid "Passed"
msgstr ""

msgid "Passed: %s"
msgstr ""

msgid "Password"
msgstr "Hasło"

msgid "Path"
msgstr ""

msgid ""
"Path to collection you wish to bind, like /user1/calendar/ or "
"https://cal.example.com/user2/cal/"
msgstr ""

msgid "Person"
msgstr ""

msgid "Please confirm deletion of access ticket - see below"
msgstr ""

msgid "Please confirm deletion of binding - see below"
msgstr ""

msgid "Please confirm deletion of collection - see below"
msgstr ""

msgid "Please confirm deletion of the principal"
msgstr ""

msgid "Please note the time and advise the administrator of your system."
msgstr ""

msgid "Principal"
msgstr ""

msgid "Principal Collections"
msgstr ""

msgid "Principal Grants"
msgstr ""

msgid "Principal ID"
msgstr ""

msgid "Principal Type"
msgstr ""

msgid "Principal deleted."
msgstr ""

msgid "Principals: Users, Resources and Groups"
msgstr ""

msgid "Privileges"
msgstr ""

msgid "Privileges granted to All Users"
msgstr ""

msgid "Privileges to allow delivery of scheduling messages"
msgstr ""

msgid "Privileges to delegate scheduling decisions"
msgstr ""

msgid "Property is read-only"
msgstr "Właściwość tylko do odczytu"

#. Translators: in the sense of being available to all users
msgid "Public"
msgstr "Publiczny"

msgid "Publicly Readable"
msgstr ""

msgid "REPORT body contains no XML data!"
msgstr ""

msgid "REPORT body is not valid XML data!"
msgstr ""

msgid "Read"
msgstr ""

msgid "Read ACLs for a resource or collection"
msgstr ""

msgid "Read Access Controls"
msgstr ""

msgid "Read Current User's Access"
msgstr ""

msgid "Read Free/Busy Information"
msgstr ""

msgid "Read the content of a resource or collection"
msgstr ""

msgid ""
"Read the details of the current user's access control to this resource."
msgstr ""

msgid "Read the free/busy information for a calendar collection"
msgstr ""

msgid "Read/Write"
msgstr ""

msgid "References"
msgstr ""

msgid "Remove"
msgstr ""

msgid "Remove a lock"
msgstr ""

msgid "Remove dangling external calendars"
msgstr ""

msgid "Report Bug"
msgstr "Zgłoś błąd"

msgid "Report a bug in the system"
msgstr "Zgłoś błąd w systemie"

msgid "Request Feature"
msgstr ""

msgid "Request body is not valid XML data!"
msgstr ""

#. Translators a thing which might be booked: a room, a carpark, a
#. projector...
msgid "Resource"
msgstr "Zasób"

msgid "Resource Calendar Principals"
msgstr ""

msgid "Resource Not Found."
msgstr ""

msgid "Resource has changed on server - not deleted"
msgstr "Zasób zmieniony na serwerze - nie usuwam."

msgid "Resources do have calendars, but they will not usually log on."
msgstr ""

msgid "Resources may not be changed to / from collections."
msgstr ""

msgid "Revoke"
msgstr ""

msgid "SRV Record"
msgstr ""

#, php-format
msgid "SRV record for \"%s\" points to wrong domain: \"%s\" instead of \"%s\""
msgstr ""

#, php-format
msgid ""
"SRV record missing for \"%s\" or DNS failure, the domain you are going to "
"send events from should have an SRV record"
msgstr ""

msgid "Schedule Deliver"
msgstr ""

msgid "Schedule Send"
msgstr ""

msgid "Schedule Transparency"
msgstr ""

msgid "Scheduling: Deliver a Reply"
msgstr ""

msgid "Scheduling: Deliver an Invitation"
msgstr ""

msgid "Scheduling: Delivery"
msgstr ""

msgid "Scheduling: Query free/busy"
msgstr ""

msgid "Scheduling: Send a Reply"
msgstr ""

msgid "Scheduling: Send an Invitation"
msgstr ""

msgid "Scheduling: Send free/busy"
msgstr ""

msgid "Scheduling: Sending"
msgstr ""

msgid "Send free/busy enquiries"
msgstr ""

msgid ""
"Send scheduling invitations as an organiser from the owner of this "
"scheduling outbox."
msgstr ""

msgid ""
"Send scheduling replies as an attendee from the owner of this scheduling "
"outbox."
msgstr ""

msgid "Set free/busy privileges"
msgstr ""

msgid "Set read privileges"
msgstr ""

msgid "Set read+write privileges"
msgstr ""

msgid ""
"Set the path to store your ics e.g. 'calendar' will be referenced as "
"/caldav.php/username/calendar/"
msgstr ""

msgid "Setup"
msgstr ""

msgid "Setup DAViCal"
msgstr ""

msgid "Should the uploaded entries be appended to the collection?"
msgstr ""

msgid "Show help on"
msgstr "Pokaż pomoc na temat"

msgid "Site Statistics"
msgstr ""

msgid "Site Statistics require the database to be available!"
msgstr ""

msgid "Some properties were not able to be changed."
msgstr "Niektóre właściwości nie mogły zostać zmienione."

msgid "Some properties were not able to be set."
msgstr ""

msgid "Source resource does not exist."
msgstr ""

msgid ""
"Special collections may not contain a calendar or other special collection."
msgstr ""

msgid "Specific Privileges"
msgstr ""

#, php-format
msgid "Stable: %s, We have: %s !"
msgstr ""

msgid "Statistics unavailable"
msgstr ""

msgid "Status"
msgstr ""

#, php-format
msgid "Status: %d, Message: %s, User: %d, Path: %s"
msgstr ""

msgid "Submit"
msgstr ""

msgid "Suhosin \"server.strip\" disabled"
msgstr ""

msgid "Sync LDAP Groups with DAViCal"
msgstr ""

msgid "Sync LDAP with DAViCal"
msgstr ""

#, php-format
msgid "TXT record corrupt for %s._domainkey.%s or DNS failure"
msgstr ""

#, php-format
msgid ""
"TXT record missing for \"%s._domainkey.%s\" or DNS failure, Private RSA key "
"is configured"
msgstr ""

msgid "Target"
msgstr ""

msgid "That destination name contains invalid characters."
msgstr ""

msgid "That resource is not present on this server."
msgstr ""

msgid ""
"The <a href=\"https://wiki.davical.org/w/Update-davical-database\">update-"
"davical-database</a> should be run manually after upgrading the software to "
"a new version of DAViCal."
msgstr ""

msgid ""
"The <a href=\"https://wiki.davical.org/w/iSchedule_configuration\">iSchedule"
" configuration</a> requires a few DNS entries. DNS SRV record(s) will need "
"to be created for all domains you wish to accept requests for, these are the"
" domain portion of email address on Principal records in DAViCal examples "
"are listed below for domains found in your database. At least 1 public key "
"must also be published if you wish to send requests from this server."
msgstr ""

msgid ""
"The <a href=\"https://www.davical.org/clients.php\">client setup page on the"
" DAViCal website</a> has information on how to configure Evolution, Sunbird,"
" Lightning and Mulberry to use remotely hosted calendars."
msgstr ""

msgid ""
"The <a href=\"https://www.davical.org/installation.php\">DAViCal "
"installation page</a> on the DAViCal website has some further information on"
" how to install and configure this application."
msgstr ""

msgid "The BIND Request MUST identify an existing resource."
msgstr ""

msgid "The BIND Request-URI MUST identify a collection."
msgstr ""

msgid "The BIND method is not allowed at that location."
msgstr ""

msgid ""
"The CalDAV:schedule-calendar-transp property may only be set on calendars."
msgstr ""

msgid "The DAViCal Home Page"
msgstr ""

msgid "The access ticket will be deleted."
msgstr ""

msgid ""
"The addressbook-query report must be run against an addressbook collection"
msgstr ""

msgid ""
"The administration of this application should be fairly simple. You can "
"administer:"
msgstr ""

msgid ""
"The administrative interface has no facility for viewing or modifying "
"calendar data."
msgstr ""

msgid "The application failed to understand that request."
msgstr ""

msgid "The application program does not understand that request."
msgstr "Aplikacja nie rozumie tego żądania."

msgid "The binding will be deleted."
msgstr ""

msgid "The calendar path contains illegal characters."
msgstr "Ścieżka kalendarza zawiera nieakceptowalne znaki."

msgid ""
"The calendar-free-busy-set is superseded by the  schedule-calendar-transp "
"property of a calendar collection."
msgstr ""

msgid "The calendar-query report may not be run against that URL."
msgstr ""

msgid ""
"The calendar-query report must be run against a calendar or a scheduling "
"collection"
msgstr ""

msgid "The collection name may not be blank."
msgstr ""

msgid "The destination collection does not exist"
msgstr ""

msgid ""
"The displayname may only be set on collections, principals or bindings."
msgstr ""

msgid ""
"The email address identifies principals when processing invitations and "
"freebusy lookups. It should be set to a unique value."
msgstr ""

msgid "The email address really should not be blank."
msgstr ""

#, php-format
msgid "The file \"%s\" is not UTF-8 encoded, please check error for more details"
msgstr ""

msgid ""
"The file is not UTF-8 encoded, please check the error for more details."
msgstr ""

msgid "The full name for this person, group or other type of principal."
msgstr ""

msgid "The full name must not be blank."
msgstr ""

msgid "The name this user can log into the system with."
msgstr "Nazwa, którą ten użytkownik może użyć do zalogowania."

msgid "The path on the server where your .ics files are."
msgstr ""

msgid "The preferred language for this person."
msgstr "Preferowany język."

msgid "The primary differences between them are as follows:"
msgstr ""

#, php-format
msgid "The principal \"%s\" does not exist"
msgstr ""

msgid "The style of dates used for this person."
msgstr "Format daty używany przez tego użytkownika."

msgid "The types of relationships that are available"
msgstr ""

msgid "The user's e-mail address."
msgstr "Adres e-mail użytkownika."

msgid "The user's full name."
msgstr "Imię i nazwisko użytkownika."

msgid "The user's password for logging in."
msgstr "Hasło logowania."

msgid "The username must not be blank, and may not contain a slash"
msgstr ""

msgid ""
"There is no ability to view and / or maintain calendars or events from "
"within this administrative interface."
msgstr ""

msgid "There was an error reading from the database."
msgstr ""

msgid "There was an error writing to the database."
msgstr "Wystąpił błąd zapisu do bazy danych."

msgid ""
"These are the things which may have collections of calendar resources (i.e. "
"calendars)."
msgstr ""

msgid ""
"This operation does the following: <ul><li>check valid groups in LDAP "
"directory</li> <li>check groups in DAViCal</li></ul> then <ul><li>if a group"
" is present in DAViCal but not in LDAP set as inactive in DAViCal</li> "
"<li>if a group is present in LDAP but not in DAViCal create the group in "
"DAViCal</li> <li>if a group in present in LDAP and DAViCal then update "
"information in DAViCal</li> </ul>"
msgstr ""

msgid ""
"This operation does the following: <ul><li>check valid users in LDAP "
"directory</li> <li>check users in DAViCal</li></ul> then <ul><li>if a user "
"is present in DAViCal but not in LDAP set him as inactive in DAViCal</li> "
"<li>if a user is present in LDAP but not in DAViCal create the user in "
"DAViCal</li> <li>if a user in present in LDAP and DAViCal then update "
"information in DAViCal</li> </ul>"
msgstr ""

msgid ""
"This page primarily checks the environment needed for DAViCal to work "
"correctly.  Suggestions or patches to make it do more useful stuff will be "
"gratefully received."
msgstr ""

msgid ""
"This process will import each file in a directory named \"username.ics\" and"
" create a user and calendar for each file to import."
msgstr ""

msgid "This server only supports the text/calendar format for freebusy URLs"
msgstr ""

msgid "Ticket ID"
msgstr ""

msgid "Time"
msgstr "Czas"

msgid "To Collection"
msgstr ""

msgid "To ID"
msgstr ""

msgid ""
"To do that you will need to use a CalDAV capable calendaring application "
"such as Evolution, Sunbird, Thunderbird (with the Lightning extension) or "
"Mulberry."
msgstr ""

msgid "Toggle all privileges"
msgstr ""

msgid "Tools"
msgstr ""

msgid "Transparent"
msgstr ""

msgid "URL"
msgstr "URL"

msgid "US Format"
msgstr ""

msgid "Unauthenticated User"
msgstr ""

msgid "United States of America (m/d/y)"
msgstr "USA (m/d/r)"

msgid "Unsupported resourcetype modification."
msgstr ""

msgid "Update"
msgstr "Aktualizuj"

msgid "Updated"
msgstr "Zaktualizowano"

msgid "Updating Collection."
msgstr ""

msgid "Updating Member of this Group Principal"
msgstr ""

msgid "Updating Principal record."
msgstr ""

msgid "Updating grants by this Principal"
msgstr ""

msgid "Upgrade DAViCal database schema"
msgstr ""

msgid "Upgrade Database"
msgstr ""

msgid "Upgrading DAViCal Versions"
msgstr ""

msgid "Upload an iCalendar file or VCard file to replace this collection."
msgstr ""

msgid "User Calendar Principals"
msgstr ""

msgid "User Details"
msgstr "Dane użytkownika"

msgid "User Functions"
msgstr ""

msgid "User Name"
msgstr "Nazwa użytkownika"

msgid "User Roles"
msgstr "Role użytkownika"

msgid "User is active"
msgstr "Użytkownik aktywny"

msgid "User record written."
msgstr "Dane użytkownika zostały zapisane."

msgid "Username"
msgstr ""

msgid "Users (or Resources or Groups) and the relationships between them"
msgstr ""

msgid ""
"Users will probably have calendars, and are likely to also log on to the "
"system."
msgstr ""

msgid "View My Details"
msgstr ""

msgid "View my own principal record"
msgstr ""

msgid "View this user record"
msgstr "Zobacz dane tego użytkownika"

msgid "Visit the DAViCal Wiki"
msgstr ""

msgid "WARNING"
msgstr ""

#, php-format
msgid "Want: %s, Currently: %s"
msgstr "Potrzebuje: %s, Aktualnie: %s"

msgid ""
"Warning: there are no active admin users! You should fix this before logging"
" out.  Consider using the $c->do_not_sync_from_ldap configuration setting."
msgstr ""

msgid "When the user's e-mail account was validated."
msgstr "Kiedy potwierdzono adres e-mail"

msgid "Write"
msgstr ""

msgid "Write ACLs for a resource or collection"
msgstr ""

msgid "Write Access Controls"
msgstr ""

msgid "Write Data"
msgstr ""

msgid "Write Metadata"
msgstr ""

msgid "Write content"
msgstr ""

msgid "Write properties"
msgstr ""

msgid "Yes"
msgstr "Tak"

msgid "You are editing"
msgstr "Edytujesz"

#, php-format
msgid "You are logged on as %s (%s)"
msgstr ""

msgid "You are not allowed to delete bindings for this principal."
msgstr ""

msgid "You are not allowed to delete collections for this principal."
msgstr ""

msgid "You are not allowed to delete principals."
msgstr ""

msgid "You are not allowed to delete tickets for this principal."
msgstr ""

msgid "You are not authorised to use this function."
msgstr "Nie masz uprawnień aby użyć tej funkcji."

msgid "You are viewing"
msgstr "Przeglądasz"

msgid ""
"You can click on any user to see the full detail for that person (or group "
"or resource - but from now we'll just call them users)."
msgstr ""

msgid "You do not have permission to modify this collection."
msgstr ""

msgid "You do not have permission to modify this record."
msgstr "Nie posiadasz uprawnień do edycji tych danych"

msgid "You may not PUT to a collection URL"
msgstr ""

msgid "You must log in to use this system."
msgstr "Musisz się zalogować."

msgid "Your configuration produced PHP errors which should be corrected"
msgstr "Twoja konfiguracja powoduje błędy PHP, które należy poprawić"

#, php-format
msgid "and create a DNS TXT record for <b>%s._domainkey.%s</b> that contains:"
msgstr ""

msgid "calendar-timezone property is only valid for a calendar."
msgstr ""

#, php-format
msgid "directory %s is not readable"
msgstr "Katalog %s nie jest do odczytu"

msgid ""
"drivers_imap_pam : imap_url parameter not configured in "
"/etc/davical/*-conf.php"
msgstr ""

msgid "drivers_ldap : Could not start TLS: ldap_start_tls() failed"
msgstr "drivers_ldap: Nie można uruchomić TLS: ldap_start_tls() spowodowało błąd"

#, php-format
msgid ""
"drivers_ldap : Failed to bind to host %1$s on port %2$s with bindDN of %3$s"
msgstr ""

msgid ""
"drivers_ldap : Failed to set LDAP to use protocol version 3, TLS not "
"supported"
msgstr ""

msgid ""
"drivers_ldap : Unable to bind to LDAP - check your configuration for bindDN "
"and passDN, and that your LDAP server is reachable"
msgstr ""

#, php-format
msgid "drivers_ldap : Unable to connect to LDAP with port %s on host %s"
msgstr ""

msgid ""
"drivers_ldap : function ldap_connect not defined, check your php_ldap module"
msgstr ""

#, php-format
msgid "drivers_pwauth_pam : Unable to find %s file"
msgstr "drivers_pwauth_pam: Nie można odnaleźć pliku %s "

msgid ""
"drivers_rimap : imap_url parameter not configured in /etc/davical/*-conf.php"
msgstr ""

#, php-format
msgid "drivers_squid_pam : Unable to find %s file"
msgstr "drivers_squid_pam: Nie można odnaleźć pliku %s "

#. Translators: this is a colloquial phrase in english (the name of a flower)
#. and is an option allowing people to log in automatically in future
msgid "forget me not"
msgstr "zapamiętaj mnie"

msgid "from principal"
msgstr ""

msgid "iSchedule Configuration"
msgstr ""

msgid "iSchedule Domains"
msgstr ""

msgid "iSchedule OK"
msgstr ""

msgid ""
"iSchedule allows caldav servers to communicate directly with each other, "
"bypassing the need to send invitations via email, for scheduled events where"
" attendees are using different servers or providers. Additionally it enables"
" freebusy lookups for remote attendees. Events and ToDos received via "
"iSchedule will show up in the users scheduling inbox."
msgstr ""

msgid "invalid request"
msgstr ""

msgid "optional"
msgstr ""

msgid "path to store your ics"
msgstr ""

msgid "please add the following section to your DAViCal configuration file"
msgstr ""

msgid "recipient must be organizer or attendee of event"
msgstr ""

msgid ""
"scheduling_dkim_domain does not match server name (please check your DAViCal"
" configuration file)"
msgstr ""

msgid ""
"scheduling_dkim_domain not set (please check your DAViCal configuration "
"file)"
msgstr ""

msgid "sender must be organizer or attendee of event"
msgstr ""

msgid "unauthenticated"
msgstr ""

msgid ""
"you should log on with the username and password that have been issued to "
"you."
msgstr "powinieneś zalogować się swoją nazwą użytkownika i hasłem."
