{
  let element = document.getElementById("fld_is_calendar");
  if (element != null ) {
    element.addEventListener('click', function() {
      toggle_enabled('fld_is_calendar','=fld_timezone','=fld_schedule_transp','!fld_is_addressbook');
    });
  }
  element = document.getElementById("fld_is_addressbook");
  if (element != null ) {
    element.addEventListener('click', function() {
      toggle_enabled('fld_is_addressbook','!fld_is_calendar');
    });
  }
  element = document.getElementById("fld_use_default_privs");
  if (element != null ) {
    element.addEventListener('click', function() {
      toggle_visible('fld_use_default_privs','!privileges_settings');
    });
  }
}

toggle_enabled('fld_is_calendar','=fld_timezone','=fld_schedule_transp','!fld_is_addressbook');
toggle_enabled('fld_is_addressbook','!fld_is_calendar');
toggle_visible('fld_use_default_privs','!privileges_settings');
