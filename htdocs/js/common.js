function toggle_visible() {
  var argv = toggle_visible.arguments;
  var argc = argv.length;

  var fld_checkbox =  document.getElementById(argv[0]);

  if ( argc < 2 ) {
    return;
  }

  for (var i = 1; i < argc; i++) {
    var block_id = argv[i].substr(1);
    var block_logical = argv[i].substr(0,1);
    var b = document.getElementById(block_id);
    if ( block_logical == '!' )
      b.style.display = (fld_checkbox.checked ? 'none' : '');
    else
      b.style.display = (!fld_checkbox.checked ? 'none' : '');
  }
}

function toggle_privileges() {
  var argv = toggle_privileges.arguments;
  var argc = argv.length;

  if ( argc < 2 ) {
    return;
  }
  var match_me = argv[0];

  var set_to = -1;
  if ( argv[1] == 'all' ) {
    let fields = document.querySelectorAll('[id^=' + match_me + '_]');
    fields.forEach((field) => {
      if ( set_to == -1 ) {
        set_to = ( field.checked ? 0 : 1 );
      }
      field.checked = set_to;
    });
  }
  else {
    for (var i = 1; i < argc; i++) {
      var f = document.getElementById( match_me + '_' + argv[i]);
      if ( f != null ) {
        if ( set_to == -1 ) {
          set_to = ( f.checked ? 0 : 1 );
        }
        f.checked = set_to;
      }
    }
  }
}

function toggle_enabled() {
  var argv = toggle_enabled.arguments;
  var argc = argv.length;

  var fld_checkbox =  document.getElementById(argv[0]);

  if ( argc < 2 ) {
    return;
  }

  for (var i = 1; i < argc; i++) {
    var fld_id = argv[i].substr(1);
    var fld_logical = argv[i].substr(0,1);
    var f = document.getElementById(fld_id);
    if ( fld_logical == '=' )
      f.disabled = !fld_checkbox.checked;
    else
      f.disabled = fld_checkbox.checked;
  }
}
